/* Program to check if the presence sensing works as intended.
 */

#include <Arduino.h>
#include <Wire.h>
#include <distance_hal.h>
#include <house2.h>

char err_buff[80] = "\n";

void setup(void) {
    Serial.begin(115200);
    Wire.begin();
    delay(500);
    int result = 0;
    result = distance_init();
    sprintf(err_buff, "initialization result: %d", result);
    Serial.println(err_buff);
}

void loop(void) {
    long now = millis();
    static int presence = 0;
    presence = read_proximity(PROXIMITY_READ_COUNTS);
    long delayed = millis() - now;
    sprintf(err_buff, "presence: %d\n", presence);
    Serial.print(err_buff);
    delay(700 - delayed);
}
