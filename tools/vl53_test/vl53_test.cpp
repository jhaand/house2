#include "SparkFun_VL53L1X.h" //Click here to get the library: http://librarymanager/All#SparkFun_VL53L1X
#include <Arduino.h>
#include <Wire.h>

/*
Inspired by code from:
  By: Nathan Seidle SparkFun Electronics Date: April 4th, 2018
  License: This code is public domain but you buy me a beer if you use this and
  we meet someday (Beerware license).
*/

#define SHUTDOWN_PIN_0 16
#define SHUTDOWN_PIN_1 17
#define INTERRUPT_PIN_0 -1
#define INTERRUPT_PIN_1 -1
SFEVL53L1X distanceSensor0(Wire, SHUTDOWN_PIN_0, INTERRUPT_PIN_0);
SFEVL53L1X distanceSensor1(Wire, SHUTDOWN_PIN_1, INTERRUPT_PIN_1);

void setup(void) {
    delay(2000);
    Wire.begin();
    Serial.begin(115200);
    Serial.println("Start VL53L1x test program. ");

    Serial.println("distance_sensors_initializing");

    int result = 0;
    distanceSensor0.sensorOff();
    delay(100);
    distanceSensor1.sensorOff();
    delay(100);
    distanceSensor0.sensorOn();
    delay(200);
    if (distanceSensor0.begin() != 0) // Begin returns 0 on a good init
    {
        Serial.println("Distance sensor 0 failed to initialize");
    } else {
        Serial.println("Distance sensor 0 initialized");
    }
    //
    // Set the new address.
    // Shift the address one bit higher because the VL53 counts the upper 7 bits
    // as the base address.
    distanceSensor0.setI2CAddress(0x32 << 1);

    distanceSensor1.sensorOn();
    delay(200);
    distanceSensor1.setI2CAddress(0x33 << 1);
    if (distanceSensor1.begin() != 0) // Begin returns 0 on a good init
    {
        Serial.println("Distance sensor 1 failed to initialize");
    } else {
        Serial.println("Distance sensor 1 initialized");
    }
    delay(200);
    Serial.println("Start main loop.");
}

void loop(void) {
    static uint16_t distance0, distance1;

    distanceSensor0.startRanging(); // Write configuration bytes to initiate
                                    // measurement
    while (!distanceSensor0.checkForDataReady()) {
        delay(1);
    }
    distance0 = distanceSensor0.getDistance(); // Get the result of the
                                               // measurement from the sensor
    byte sensor0_state = distanceSensor0.getRangeStatus();

    distanceSensor0.clearInterrupt();
    distanceSensor0.stopRanging();

    distanceSensor1.startRanging(); // Write configuration bytes to initiate
                                    // measurement
    // Check sensor 1
    while (!distanceSensor1.checkForDataReady()) {
        delay(1);
    }
    distance1 = distanceSensor1.getDistance(); // Get the result of the
                                               // measurement from the sensor
    byte sensor1_state = distanceSensor1.getRangeStatus();

    distanceSensor1.clearInterrupt();
    distanceSensor1.stopRanging();

    static char buffer[120];
    sprintf(buffer,
            "Sensor 0: %4d mm, sensor0_state: %1d  |  Sensor 1: %4d mm, "
            "sensor1_state: %1d ",
            distance0, sensor0_state, distance1, sensor1_state);
    Serial.println(buffer);
    delay(200);
}
