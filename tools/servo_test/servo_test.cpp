#include <Arduino.h>
#include <servoshield_hal.h>
#include <stdint.h>

void setup(void) {
    Serial.begin(115200);
    Serial.println("16 channel Servo test.");
    int result = servo_init();
    delay(2000);
}

void loop(void) {
    static char buffer[120];
    servo_actuator_t sa = {0, 1};
    Serial.println("Start servo");
    for (int i = 104; i > 14; i--) {
        sprintf(buffer, "Servo value: %d\n", i);
        Serial.print(buffer);
        set_servo_pos(&sa, i);
        delay(50);
    }

    set_servo_off(&sa);
    while (1)
        ;
}
