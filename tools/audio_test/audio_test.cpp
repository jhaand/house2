#include <Arduino.h>
#include <Wire.h>
#include <mcp_gpio_hal.h>

void setup(void) {
    int status = 0;
    delay(3000);
    Serial.println("------------------------------------");
    Serial.println("Start GPIO (Audio and fan) tool");
    Serial.begin(115200);
    Wire.begin();
    Serial.println("Initialize the GPIO expanders");
    mcp_gpio_init();
    if (status != 0) {
        Serial.println("MCP23017 initialization failed.");
        Serial.print("Status: ");
        Serial.print(status);
        Serial.println(" ");
    }
}

void loop(void) {
    Serial.println("Start speech for room 44");
    mcp_audio_speech(1);
    delay(10000);
    mcp_audio_speech(0);
    delay(1000);

    Serial.println("Start music");
    mcp_start_audio_music();
    delay(20000);
    delay(1000);

    Serial.println("Start wind");
    Serial.println("Start the fan");
    mcp_set_fan(1);
    mcp_start_audio_wind();
    delay(20000);
    Serial.println("Stop the fan");
    mcp_set_fan(0);
    delay(1000);

    Serial.println("Start fireworks");
    mcp_start_audio_fireworks();
    delay(20000);
    Serial.println("Stop demo.");
    while (1) {
        ;
    }
    Serial.println("Never get here.");
}
