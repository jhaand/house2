#include <Arduino.h>
#include <Wire.h>
#include <house2.h>
#include <servoshield_algo.h>
#include <servoshield_hal.h>

void setup(void) {
    Serial.begin(115200);
    Serial.setTimeout(10000);
    char input_buffer[40];
    Serial.println("Servo trajectory test.");
    int result = servo_init();
    Serial.println("Servo init passed.");
    delay(2000);
}

void loop(void) {
    static char buffer[160];
    Serial.println("Buffer initialized");
    Serial.flush();
    servo_trajectory_t st;
    for (int i = 1; i < 45; i++) {
        room_number_t room_index = get_servo_settings(i, &st);
        sprintf(
            buffer,
            "room index: %d, servoshield: %d, servo: %d, closed: %d, open: %d",
            room_index, st.servo.servoshield, st.servo.actuator, st.closed,
            st.open);
        Serial.println(buffer);
        Serial.println("Start trajectory");
        servo_trajectory_play(&st);
    }

    while (1)
        ;
}
