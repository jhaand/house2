// Based on the sketch from:
// https://github.com/adafruit/Adafruit-PWM-Servo-Driver-Library/blob/master/examples/servo/servo.ino
// (c) Jelle Haandrikman 2022, @jhaand
//
#include <Adafruit_PWMServoDriver.h>
#include <Wire.h>

#define SERVO_SHIELD_ADDR 0x40
#define SERVO_NUM 0
#define SERVO_OSCFREQ 25000000

#define SERVOMIN 102
#define SERVOMID 307
#define SERVOMAX 491
#define SERVO_FREQ 50
#define SERV_DELAY 5
#define DEGREESMIN 0
#define DEGREESMID 90
#define DEGREESMAX 180

Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver(SERVO_SHIELD_ADDR);

uint16_t degrees2pwm(uint8_t degrees) {
    return map(degrees, 0, 180, SERVOMIN, SERVOMAX);
}

void setup() {
    Serial.begin(115200);
    Serial.println("16 channel Servo test.");

    pwm.begin();
    pwm.setOscillatorFrequency(SERVO_OSCFREQ);
    pwm.setPWMFreq(SERVO_FREQ);

    delay(10);
}

void loop() {
    static uint8_t servonum = SERVO_NUM;

    // Go to the lowest position, mid and end position
    uint16_t angle = DEGREESMIN;
    pwm.setPWM(servonum, 0, degrees2pwm(angle));
    Serial.println(angle);
    delay(2000);
    angle = DEGREESMID;
    pwm.setPWM(servonum, 0, degrees2pwm(angle));
    Serial.println(angle);
    delay(2000);
    angle = DEGREESMAX;
    pwm.setPWM(servonum, 0, degrees2pwm(angle));
    Serial.println(angle);
    delay(2000);
    pwm.setPWM(servonum, 0, 0);
    Serial.println(0);
    delay(2000);

    // Sweep from max to min, max and min
    delay(50);
    for (uint16_t pulselen = SERVOMAX; pulselen > SERVOMIN; pulselen--) {
        pwm.setPWM(servonum, 0, pulselen);
        delay(SERV_DELAY);
    }
    for (uint16_t pulselen = SERVOMIN; pulselen < SERVOMAX; pulselen++) {
        pwm.setPWM(servonum, 0, pulselen);
        delay(SERV_DELAY);
    }
    delay(50);
    for (uint16_t pulselen = SERVOMAX; pulselen > SERVOMIN; pulselen--) {
        pwm.setPWM(servonum, 0, pulselen);
        delay(SERV_DELAY);
    }
}
