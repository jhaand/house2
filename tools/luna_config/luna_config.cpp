#include <Arduino.h>
#include <TFLI2C.h>
#include <Wire.h>
#include <stdint.h>

TFLI2C tfli2C;
static int16_t tfAddr = TFL_DEF_ADR;

/* Setup the NodeMCU to enable the i2c and Serial port
 */

void setup() {
    Wire.begin();         // Initialize the Wire library
    delay(1000);          // Wait for the serial port monitor to come up
    Serial.begin(115200); // Start serial port with 115200 baud
}

void find_I2C() {
    Serial.println("Find the I2C adresses");
    byte error, address;
    int nDevices = 0;
    Serial.println("\nScanning....");
    for (address = 1; address < 127; address++) {
        Wire.beginTransmission(address);
        error = Wire.endTransmission();
        if (error == 0) {
            Serial.print("I2C device found at address 0x");
            if (address < 16) {
                Serial.println("0");
            }
            Serial.println(address, HEX);
            nDevices++;
        } else if (error == 4) {
            Serial.print("Unknown error at address 0x");
            if (address < 16) {
                Serial.print("0");
            }
            Serial.println(address, HEX);
        }
    }
    if (nDevices == 0) {
        Serial.println("No I2C devices found\n");
    } else {
        Serial.println("done\n");
    }
}

void select_luna() {
    char buffer[80];
    Serial.println("Current address of the TF-Luna");
    sprintf(buffer, "TF-Luna number: 0x%x, decimal: %d", tfAddr, tfAddr);
    Serial.println("Select the Luna to measure or reprogram");
    Serial.println("You have 10 seconds.");
    Serial.setTimeout(10000);
    Serial.readBytesUntil('\n', buffer, 5);
    uint16_t select_address = (uint16_t)strtol(buffer, NULL, 0);
    sprintf(buffer, "Read number: 0x%x, decimal: %d", select_address,
            select_address);
    Serial.println(buffer);
    // Reset the serial timeout back to 1000 msec.
    Serial.setTimeout(1000);

    bool address_ok = true;
    if (select_address < 0x08) {
        Serial.println("The address is too low.");
        address_ok = false;
    }
    if (select_address > 0x77) {
        Serial.println("The address is too high");
        address_ok = false;
    }
    if (address_ok == true) {
        tfAddr = select_address;
        Serial.println("Selected TF-Luna");
    }
    Serial.println("Press Enter to continue");
    select_address = Serial.read();
    Serial.println("OK, we're done.");
}

void program_luna() {
    char buffer[80];
    Serial.println("Current address of the TF-Luna");
    sprintf(buffer, "TF-Luna number: 0x%x, decimal: %d", tfAddr, tfAddr);
    Serial.println("Program the I2C address.");
    Serial.println("Enter the new address. Hex numbers allowed. 0x08 .. 0x77");
    Serial.println("You have 10 seconds.");
    Serial.setTimeout(10000);
    Serial.readBytesUntil('\n', buffer, 5);
    uint16_t new_address = (uint16_t)strtol(buffer, NULL, 0);
    sprintf(buffer, "Read number: 0x%x, decimal: %d", new_address, new_address);
    Serial.println(buffer);
    // Reset the serial timeout back to 1000 msec.
    Serial.setTimeout(1000);
    bool address_ok = true;
    if (new_address < 0x08) {
        Serial.println("The address is too low.");
        address_ok = false;
    }
    if (new_address > 0x77) {
        Serial.println("The address is too high");
        address_ok = false;
    }
    if (address_ok == true) {
        tfli2C.Set_I2C_Addr((uint8_t)new_address, tfAddr);
        delay(100);
        tfli2C.Save_Settings(tfAddr);
        delay(10);
        tfli2C.Soft_Reset(tfAddr);
        delay(5000); // Do nothing with the sensor while reprogramming for 10
                     // seconds
        tfAddr = new_address;
        // It seems we need to do this 2 times
        tfli2C.Set_I2C_Addr((uint8_t)new_address, tfAddr);
        delay(100);
        tfli2C.Save_Settings(tfAddr);
        delay(10);
        tfli2C.Soft_Reset(tfAddr);
        delay(1000); // Do nothing with the sensor while reprogramming for 10
                     // seconds
        Serial.println("Reprogrammed TF-Luna");
    }
    new_address = Serial.read();
    Serial.println("OK, we're done.");
}

void read_luna() {
    char buffer[80];
    static uint16_t tfFrame; // Frame Rate
    static int16_t tfDist;   // Distance in centimeters
    static int16_t tfTemp;   // Temperature in degrees Centigrade
    static int16_t tfFlux;   // Confidence value
    uint8_t revs[3];         // 3 bytes with revision numbers

    if (tfli2C.Get_Frame_Rate(tfFrame, tfAddr)) {
        Serial.print("Frame rate: ");
        Serial.println(tfFrame);
    }
    if (tfli2C.Get_Firmware_Version(revs, tfAddr)) {
        sprintf(buffer, "Version: %d.%d.%d", revs[2], revs[1], revs[0]);
        Serial.println(buffer);
    }
    int index = 20;
    while (index > 0) {
        if (tfli2C.getData(tfDist, tfFlux, tfTemp, tfAddr)) // If read okay
        {
            uint16_t tempwhole = tfTemp / 100;
            uint16_t tempfrac = tfTemp % 100;
            sprintf(buffer, "Distance: %4d, Flux: %4d, Temp: %2d.%02d\n",
                    tfDist, tfFlux, tempwhole, tempfrac);
            Serial.print(buffer);
        } else {
            tfli2C.printStatus();
        }
        delay(500);
        index--;
    }
}

void luna_hard_reset() {
    Serial.println("*** Hard reset the TF-luna. ***\n");
    tfli2C.Hard_Reset(tfAddr);
}

void luna_soft_reset() {
    Serial.println("Soft reset the TF-luna.");
    tfli2C.Soft_Reset(tfAddr);
}

/*
   Main program with a menu via the serial port
*/

void loop() {
    static char buffer[80];
    Serial.println(
        "----------------------------------------------------------------");
    Serial.println("The TF-luna configuration program");
    Serial.println("Choose one of the following options:");
    Serial.println(" 1: Find the connected i2c devices.");
    Serial.println(" 2: Select the TF-Luna to query.");
    Serial.println(
        " 3: Check the measured distance and temperature for 10 seconds.");
    Serial.println(" 4: Reprogram the found device to a different address.");
    Serial.println(" 5: Hard reset the TF-Luna.");
    Serial.println(" 6: Soft reset of the TF-Luna.");
    sprintf(buffer, "\nCurrent addresss: 0x%x, decimal: %d", tfAddr, tfAddr);
    Serial.println(buffer);
    Serial.println("Select item:");

    while (!Serial.available()) {
    }
    switch (Serial.read()) {
    case '1':
        find_I2C();
        break;
    case '2':
        select_luna();
        break;
    case '3':
        read_luna();
        break;
    case '4':
        program_luna();
        break;
    case '5':
        luna_hard_reset();
        break;
    case '6':
        luna_soft_reset();
        break;
    default:
        Serial.println(" - Invalid command. Try again.");
        break;
    }
}
