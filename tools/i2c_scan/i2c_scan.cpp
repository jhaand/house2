#include <Arduino.h>
#include <Wire.h>
#include <distance_hal.h>

void setup() {
    char buffer[120];
    delay(3000);
    Serial.println(
        "Programming for scanning a configured I2C bus for House 2.");
    Wire.begin();
    Wire.setClock(400000);
    Serial.begin(115200);

    Serial.println("Initialize the VL53L1x distance sensors. ");
    int result = distance_init();
    sprintf(buffer, "VL52X1 init results: %d", result);
    Serial.println(buffer);
}

void loop() {
    Serial.println("Scan the I2C bus");
    byte error, address;
    int nDevices = 0;

    Serial.println("\nScanning.....");
    for (address = 1; address < 127; address++) {
        Wire.beginTransmission(address);
        error = Wire.endTransmission();
        if (error == 0) {
            Serial.print("I2C device found at address 0x");
            if (address < 16) {
                Serial.print("0");
            }
            Serial.println(address, HEX);
            nDevices++;
        } else if (error == 4) {
            Serial.print("Unknown error at address 0x");
            if (address < 16) {
                Serial.print("0");
            }
            Serial.println(address, HEX);
        }
    }

    if (nDevices == 0) {
        Serial.println("No I2C devices found\n");
    } else {
        Serial.println("Done.");
    }
    delay(3000);
}
