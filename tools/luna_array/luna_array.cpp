#include <Arduino.h>
#include <Wire.h>
#include <sensorbox_algo.h>
#include <sensorbox_hal.h>
#include <stdint.h>

//#define DEBUG
luna_array_t luna_array;

#ifdef DEBUG
char err_buffer[80] = "\n";
#endif

void setup() {
    char buffer[200];
    Wire.begin();
    delay(100);
    Serial.begin(115200);
    Serial.println("End Setup");

    sensorbox_init(&luna_array);
}

void loop() {
    static char buffer[80];

    static volatile sensor_array_t sa = {0, 0, 0, 0, 0};
    static volatile room_location_t rl = {0, 0};

    sensorbox_read(&luna_array, &sa);

    int status = get_location(&sa, &rl);

    uint8_t row = rl.row;
    uint8_t column = rl.column;
    room_number_t room = select_room(&rl, 5);
#ifdef DEBUG
    Serial.print(err_buffer);
#endif
    sprintf(
        buffer,
        "sensor distances: %5d %5d %5d %5d %5d: column  %d row %d  %2d   \r",
        sa.sensor[0], sa.sensor[1], sa.sensor[2], sa.sensor[3], sa.sensor[4],
        column, row, room);
    Serial.print(buffer);
    delay(200);
}
