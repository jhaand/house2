// Test program for MCP23017 IO extender over I2C
// (c) Jelle Haandrikman @jhaand, 2022
//

#include "MCP23017.h"
#include <Wire.h>

MCP23017 MCP0(0x20);
MCP23017 MCP1(0x21);

byte mcp0_present = 0;
byte mcp1_present = 0;

void setup() {
    Serial.begin(115200);
    Serial.println("Test program for MCP23017 for House 2");

    Wire.begin();
    MCP0.begin(21, 22);
    MCP1.begin(21, 22);
    if (MCP0.isConnected()) {
        Serial.println("Connected to IO extender 0x20");
        mcp0_present = 1;
    } else {
        Serial.println("IO extender on 0x20 missing");
    }
    if (MCP0.isConnected()) {
        Serial.println("Connected to IO extender 0x21");
        mcp1_present = 1;
    } else {
        Serial.println("IO extender on 0x21 missing");
    }

    Serial.println("Set all ports to output.");
    if (mcp0_present == 1) {
        MCP0.pinMode16(0x0003);
        MCP0.write16(0x0000);
    }
    if (mcp1_present == 1) {
        MCP1.pinMode16(0x0003);
        MCP1.write16(0xffff);
    }
    delay(1000);
}

void loop() {
    // static char buffer[80];
    static byte o = 0;
    byte i;
    o = (o == 0) ? 1 : 0;
    Serial.print("Set all ports to: ");
    Serial.println(o);
    if (mcp0_present == 1) {
        for (i = 2; i < 16; i++) {
            MCP0.digitalWrite(i, o);
            delay(100);
        }
    }
    if (mcp1_present == 1) {
        for (i = 2; i < 16; i++) {
            MCP1.digitalWrite(i, o);
            delay(100);
        }
    }
}
