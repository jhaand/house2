#include <Arduino.h>
#include <Wire.h>

#define STATUS_L0 00
#define STATUS_L1 04
#define ERROR_L 15
// LED_BUILTIN

void setup(void) {
    pinMode(LED_BUILTIN, OUTPUT);
    pinMode(STATUS_L0, OUTPUT);
    pinMode(STATUS_L1, OUTPUT);
    pinMode(ERROR_L, OUTPUT);

    digitalWrite(LED_BUILTIN, LOW);
    digitalWrite(STATUS_L0, LOW);
    digitalWrite(STATUS_L1, LOW);
    digitalWrite(ERROR_L, LOW);
    delay(1000);
}

void loop(void) {
    digitalWrite(LED_BUILTIN, HIGH);
    delay(300);
    digitalWrite(STATUS_L0, HIGH);
    delay(300);
    digitalWrite(STATUS_L1, HIGH);
    delay(300);
    digitalWrite(ERROR_L, HIGH);
    delay(1300);

    digitalWrite(LED_BUILTIN, LOW);
    digitalWrite(STATUS_L0, LOW);
    digitalWrite(STATUS_L1, LOW);
    digitalWrite(ERROR_L, LOW);
    delay(500);
}
