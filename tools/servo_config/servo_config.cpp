#include <Arduino.h>
#include <servoshield_algo.h>
#include <servoshield_hal.h>

void setup() {
    Wire.begin();         // Initialize the Wire library
    delay(1000);          // Wait for the serial port monitor to come up
    Serial.begin(115200); // Start serial port with 115200 baud
    servo_init();
}

room_number_t select_room(room_number_t room) {
    char buffer[80];
    Serial.println("Select the room number (1-44);");
    Serial.setTimeout(10000);
    Serial.readBytesUntil('\n', buffer, 5);
    room_number_t room_new = (uint16_t)strtol(buffer, NULL, 0);

    sprintf(buffer, "Read number: decimal: %d", room_new);
    if ((room_new < 1) || (room_new > 44)) {
        Serial.print("input has illegal value:");
        sprintf(buffer, "Selected value: %d", room_new);
        Serial.println(buffer);
        room_new = room;
        sprintf(buffer, "Reset room number to old value: %d", room_new);
        Serial.println(buffer);
    }
    // Reset the serial timeout back to 1000 msec.
    Serial.setTimeout(1000);

    return room_new;
}

void select_shield(servo_trajectory_t *st) {
    char buffer[80];
    Serial.println("* Select the shield number (0-3);");
    Serial.setTimeout(10000);
    Serial.readBytesUntil('\n', buffer, 5);
    servoshield_idx_t shield_new = (uint16_t)strtol(buffer, NULL, 0);

    sprintf(buffer, "Read number: decimal: %d", shield_new);
    if ((shield_new >= 0) && (shield_new <= 3)) {
        st->servo.servoshield = shield_new;
    } else {
        Serial.print(" - Input has illegal value:");
        sprintf(buffer, "   Selected value: %d", shield_new);
        Serial.println(buffer);
        sprintf(buffer, "   Shield number is kept to old value: %d",
                st->servo.servoshield);
        Serial.println(buffer);
    }
    // Reset the serial timeout back to 1000 msec.
    Serial.setTimeout(1000);
}

void select_actuator(servo_trajectory_t *st) {
    char buffer[40];
    Serial.println("* Select the servo number (0-15);");
    Serial.setTimeout(10000);
    Serial.readBytesUntil('\n', buffer, 5);
    servo_t servo_new = (uint16_t)strtol(buffer, NULL, 0);

    sprintf(buffer, "Read number: decimal: %d", servo_new);
    if ((servo_new < 0) && (servo_new > 3)) {
        Serial.print(" - Input has illegal value:");
        sprintf(buffer, "   Selected value: %d", servo_new);
        Serial.println(buffer);
        sprintf(buffer, "   Servo number is kept to old value: %d",
                st->servo.servoshield);
        Serial.println(buffer);
    } else {
        st->servo.actuator = servo_new;
    }
    // Reset the serial timeout back to 1000 msec.
    Serial.setTimeout(1000);
}

void tune_actuator(servo_trajectory_t *st) {
    static char buffer[120];
    int16_t position = st->closed;
    servo_actuator_t servo = st->servo;
    uint8_t doorgaan = 1;
    Serial.println("Tune the Servo for a single room. ");
    Serial.println(
        "Commands: (q)uit, move with: <,.> (c)lose position, (o)pen position");
    do {
        sprintf(buffer, "Servo %2d.%2d, position: %d", st->servo.servoshield,
                st->servo.actuator, position);
        Serial.println(buffer);
        set_servo_pos(&servo, position);
        while (!Serial.available()) {
        }
        switch (Serial.read()) {
        case 'q':
            doorgaan = 0;
            break;
        case '<':
            position += 10;
            break;
        case ',':
            position += 1;
            break;
        case '.':
            position -= 1;
            break;
        case '>':
            position -= 10;
            break;
        case 'o':
            Serial.println("Stored Open position.");
            st->open = position;
            break;
        case 'c':
            Serial.println("Stored Closed position.");
            st->closed = position;
            break;
        default:
            Serial.println("Unknown command");
            break;
        }
        if (position < 0) { // Take care of overflow
            position = 0;
        }
        if (position > 180) {
            position = 180;
        }
        sprintf(buffer, "", position);
        set_servo_pos(&servo, position);
    } while (doorgaan != 0);
    set_servo_off(&servo);
}

void run_trajectory(servo_trajectory_t *st) {
    Serial.println("Start Trajectory");
    servo_trajectory_play(st);
    Serial.println("End Trajectory\n");
}

void loop(void) {
    static char buffer[120];
    static room_number_t current_room = 1;
    static room_number_t room_index;
    static servo_trajectory_t st = {0, 0, 175,
                                    65}; // Configure as the first room.

    Serial.println(
        "----------------------------------------------------------------");
    Serial.println("The Servoshield configuration program");
    Serial.println("Choose one of the following options:");
    Serial.println(" 1: Select room.  (1-44)");
    Serial.println(" 2: Select servoshield. (0-3)");
    Serial.println(" 3: Select servo actuator. (0-15)");
    Serial.println(" 4: Interactive tuning of Room.");
    Serial.println(" 5: Run predefined trajectory.");
    sprintf(buffer,
            "selected room: %d, room index: %d, servoshield: %d, servo: %d, "
            "closed: %d, open: %d",
            current_room, current_room - 1, st.servo.servoshield,
            st.servo.actuator, st.closed, st.open);
    Serial.println(buffer);
    while (!Serial.available()) {
    }
    switch (Serial.read()) {
    case '1':
        current_room = select_room(current_room);
        get_servo_settings(current_room, &st);
        break;
    case '2':
        select_shield(&st);
        break;
    case '3':
        select_actuator(&st);
        break;
    case '4':
        tune_actuator(&st);
        break;
    case '5':
        run_trajectory(&st);
        break;
    default:
        Serial.println(" - Invalid command. Try again.");
        break;
    }
}
