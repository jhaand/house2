// Main program for House 2
// (c) Jelle Haandrikman @jhaand, 2022
//

// Standard Arduino include files.
#include <Arduino.h>
#include <Wire.h>
#include <esp_task_wdt.h>
#include <stdint.h>

// My own libraries.
#include <distance_hal.h>
#include <house2.h>
#include <mcp_gpio_hal.h>
#include <sensorbox_algo.h>
#include <sensorbox_hal.h>
#include <servoshield_algo.h>
#include <servoshield_hal.h>

// We're going to Kunstmaand Ameland
#define LOC_AMELAND

#define WDT_TIMEOUT 42
luna_array_t luna_array;

typedef long time_t;
enum states { waiting, speech, rooms_active };

// / Setup
void setup() {
    // Wait for the Serial monitor to start.
    delay(1000);
    // Initialize the I2C bus and serial bus.
    Wire.begin();
    char buffer[120];
    Serial.begin(115200);
    Serial.println("Start main program for House 2");
    Serial.println("Jelle Haandrikman, @jhaand, 2022");

    Serial.println("Configuring Watchdog.");
    esp_task_wdt_init(WDT_TIMEOUT, true);
    esp_task_wdt_add(NULL);

    // Intialize the sensorbox
    int result = sensorbox_init(&luna_array);
    sprintf(buffer, "Sensorbox init result: %d", result);
    Serial.println(buffer);

    // Initialize the VL53L1x's first, because they willnot show up otherwise.
    result = distance_init();
    sprintf(buffer, "VL53X1 init results: %d", result);
    Serial.println(buffer);

    // Servoshield
    result = servo_init();
    sprintf(buffer, "Servoshield expander results: %d", result);
    Serial.println(buffer);

    // Initialize the MCP23017 GPIO expander.
    result = mcp_gpio_init();
    sprintf(buffer, "MCP23017 GPIO expander results: %d", result);
    Serial.println(buffer);

    // Check adresses for TF-luna, VL53L1x, IO-shield, Servo-shield
    //  Do a quick scan of the i2C bus.
    // i2c_scan();
    //
    Serial.println("Close all the hatches. ");
    servo_trajectory_t st;

    room_number_t room_index = 0;

    // Close all the rooms.
    Serial.println("Pet the watchdog at each room closing.");
    for (int i = 1; i < 45; i++) {
        room_index = get_servo_settings(i, &st);
        servo_trajectory_shut(&st);
        esp_task_wdt_reset();
    }
}

#ifdef LOC_AMELAND
#define TIME_SPEECH_LENGTH 59500 // Kunstmaand Ameland speech
#else
#define TIME_SPEECH_LENGTH 67000 // Franz Pfanner House speech
#endif

void loop() {
    static bool first = 1;
    static states state = waiting;
    static time_t time_speech_end;
    static const time_t time_speech_length = TIME_SPEECH_LENGTH;

    static time_t time_watchdog_pet;
    static const time_t time_watchdog_length = 4950;

    static room_number_t room_index = 0;
    static servo_trajectory_t st;

    static time_t time_proximity_end = 0;
    static const time_t time_proximity_length = 60000;
    static const uint8_t proximity_count = PROXIMITY_READ_COUNTS;

    // Threshold for actually selecting a room for House2. Since this loop will
    // run every 100 msec, this will mean that we need 300 msec of steady
    // readings before returning a room.
    static const uint8_t luna_counts = 3;

    time_t now = millis();
    char buffer[120];
    if (first == 1) {
        first = 0;
        sprintf(buffer, "Starting main loop");
        Serial.println(buffer);
        Serial.println("State is: waiting");
    }
    // Check if a room is triggered. Except for room 12.
    static volatile sensor_array_t sa = {0, 0, 0, 0, 0};
    static volatile room_location_t rl = {0, 0};

    sensorbox_read(&luna_array, &sa);

    int status = get_location(&sa, &rl);

    uint8_t row = rl.row;
    uint8_t column = rl.column;
    room_index = select_room(&rl, luna_counts);

    if (room_index == 12) {
        room_index = 0;
    }

    uint8_t proximity = 0;

    if ((room_index >= 1) && (room_index <= 70)) {
        sprintf(buffer,
                "\nsensor distances: %5d %5d %5d %5d %5d: column  %d row %d  "
                "%2d   \n",
                sa.sensor[0], sa.sensor[1], sa.sensor[2], sa.sensor[3],
                sa.sensor[4], column, row, room_index);
        Serial.print(buffer);
        if (room_index <= 44) {
            proximity = 1;
        }
    } else {
        // Check if someone is near.
        proximity = read_proximity(proximity_count);
    }

    if (state == waiting) {
        if ((proximity == 1) || ((room_index >= 1) && (room_index <= 44))) {
            if (proximity == 1) {
                Serial.println(" Person in front of the house.");
            }
            sprintf(buffer, "room_index: %d", room_index);
            Serial.println(buffer);

            Serial.println(" Switching to state: speech");
            state = speech;
            now = millis(); // Start the speech
            time_speech_end = time_speech_length + now;
            mcp_audio_speech(1);
            get_servo_settings(12, &st); // Open door of room no. 12
            servo_trajectory_open(&st);
        }
    }

    if (state == speech) {
        if ((now > time_speech_end) ||
            ((room_index >= 1) && (room_index <= 44))) {
            Serial.println("End the speech");
            mcp_audio_speech(0);
            Serial.println("State is: room_active");
            state = rooms_active;
            time_proximity_end = now + time_proximity_length;
        }
    }

    if (state == rooms_active) {
        if ((room_index >= 1) && (room_index <= 44)) {
            Serial.println("Pet the watchdog. (active) ");
            esp_task_wdt_reset();
            time_proximity_end = now + time_proximity_length;
            room_special_case_start(room_index);
            get_servo_settings(room_index, &st);
            servo_trajectory_play(&st);
            room_special_case_stop(room_index);
        } else if (proximity == 1) {
            time_proximity_end = now + time_proximity_length;
        }

        if (now > time_proximity_end) {
            Serial.println("Pet the watchdog. (active) ");
            esp_task_wdt_reset();
            Serial.println("Switching to state: waiting");
            state = waiting;
            get_servo_settings(12, &st);
            servo_trajectory_close(&st);
        }
    }

    if (now > time_watchdog_pet) {
        sprintf(buffer,
                "%08ld.%03ld, proximity_end: %08ld.%03ld, Pet the watchdog. "
                "proximity: %d, "
                "room_index: %d",
                (now / 1000), (now % 1000), (time_proximity_end / 1000),
                (time_proximity_end % 1000), proximity, room_index);
        Serial.println(buffer);
        esp_task_wdt_reset();
        time_watchdog_pet = now + time_watchdog_length;
    }
    delay(50);
}
