#!/bin/bash

find lib/ test/ src/ tools/ -name \*.cpp -o -name \*.h  |xargs clang-format -i
