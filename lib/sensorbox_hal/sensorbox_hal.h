#ifndef SENSORBOX_HAL_H
#define SENSORBOX_HAL_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef SENSOR_ARRAY_T
#define SENSOR_ARRAY_T
typedef struct {
    int sensor[5];
} sensor_array_t;
#endif

typedef struct {
    int16_t address;
    int16_t dist;
    int16_t temp;
    int16_t flux;
} luna_sensor_t;

typedef struct {
    luna_sensor_t luna_sensor[5];
} luna_array_t;

int sensorbox_init(luna_array_t *);
int sensorbox_read(luna_array_t *, sensor_array_t *);

#ifdef __cplusplus
}
#endif

#endif
