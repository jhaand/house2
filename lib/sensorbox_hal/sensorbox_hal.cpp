#include <Arduino.h>
#include <TFLI2C.h>
#include <Wire.h>
#include <sensorbox_hal.h>
#include <stdint.h>

TFLI2C tfli2C;

int sensorbox_init(luna_array_t *la) {
    la->luna_sensor[0].address = 0x60;
    la->luna_sensor[1].address = 0x61;
    la->luna_sensor[2].address = 0x62;
    la->luna_sensor[3].address = 0x63;
    la->luna_sensor[4].address = 0x64;

    int luna_array_len = sizeof(la->luna_sensor) / sizeof(la->luna_sensor[0]);
    for (int i = 0; i < luna_array_len; i++) {
        tfli2C.Soft_Reset(la->luna_sensor[i].address);
        delay(50);
    }
    return 0;
}

int sensorbox_read(luna_array_t *la, sensor_array_t *sa) {
    int status;
    int luna_array_len = sizeof(la->luna_sensor) / sizeof(la->luna_sensor[0]);
    for (int i = 0; i < luna_array_len; i++) {
        if (tfli2C.getData(la->luna_sensor[i].dist, la->luna_sensor[i].flux,
                           la->luna_sensor[i].temp,
                           la->luna_sensor[i].address)) {
            status = 0;
            sa->sensor[i] = (int)la->luna_sensor[i].dist * 10;
        } else {
            status = -1;
        }
        delay(10);
    }
    return status;
}
