#include <Adafruit_PWMServoDriver.h>
#include <Arduino.h>
#include <Wire.h>
#include <house2.h>
#include <stdint.h>

#ifndef SERVOSHIELD_HAL_H
#define SERVOSHIELD_HAL_H

#ifdef __cplusplus
extern "C" {
#endif

#ifndef SERVO_TRAJECTORY_T
#define SERVO_TRAJECTORY_T
typedef uint8_t servoshield_idx_t;
typedef uint8_t servo_t;
typedef uint8_t degrees_t;

typedef struct {
    servoshield_idx_t servoshield; // i2c address of the servoshield
    servo_t actuator;              // the index of the servo to control.
} servo_actuator_t;

typedef struct {
    servo_actuator_t servo;
    degrees_t closed;
    degrees_t open;
} servo_trajectory_t;
#endif

int servo_init(void);

int servo_trajectory_play(servo_trajectory_t *);

int servo_trajectory_open(servo_trajectory_t *);

int servo_trajectory_close(servo_trajectory_t *);

int servo_trajectory_shut(servo_trajectory_t *);

int set_servo_pos(servo_actuator_t *, degrees_t degrees);

int set_servo_off(servo_actuator_t *);

uint16_t degrees2pwm(degrees_t degrees);

#ifdef __cplusplus // cplusplus
}
#endif

#endif // SERVOSHIELD_ALGO_H
