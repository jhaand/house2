#include <Adafruit_PWMServoDriver.h>
#include <Arduino.h>
#include <Wire.h>
#include <esp_task_wdt.h>
#include <house2.h>
#include <servoshield_hal.h>

#define SERVOMIN 102
#define SERVOMID 307
#define SERVOMAX 491
#define SERVO_FREQ 50
#define SERV_DELAY_TIME 50
#define DEGREESMIN 0
#define DEGREESMID 90
#define DEGREESMAX 180

#define PAUSE_TIME_CLOSED 200
#define PAUSE_TIME_OPEN 9000
#define PAUSE_TIME_MID 1000

const degrees_t mid_offset = 15;

Adafruit_PWMServoDriver pwm1 = Adafruit_PWMServoDriver(0x40);
Adafruit_PWMServoDriver pwm2 = Adafruit_PWMServoDriver(0x41);
Adafruit_PWMServoDriver pwm3 = Adafruit_PWMServoDriver(0x42);
Adafruit_PWMServoDriver pwm4 = Adafruit_PWMServoDriver(0x43);

Adafruit_PWMServoDriver servoshields[] = {pwm1, pwm2, pwm3, pwm4};

int servo_init(void) {
    const uint32_t servo_oscfreq[] = {25000000, 25000000, 25000000, 25000000};
    uint8_t servoshields_len = sizeof(servoshields) / sizeof(servoshields[0]);
    for (uint8_t i = 0; i < servoshields_len; i++) {
        servoshields[i].begin();
        servoshields[i].setOscillatorFrequency(servo_oscfreq[i]);
        servoshields[i].setPWMFreq(SERVO_FREQ);
        delay(10);
    }
    return 0;
}

int servo_trajectory_play(servo_trajectory_t *st) {
    servo_actuator_t sa = st->servo;
    degrees_t closed = st->closed;
    degrees_t open = st->open;
    degrees_t position = closed;
    // Bail if the angle is too small.
    if (closed - mid_offset < open) {
        return -1;
    }
    degrees_t mid = closed - mid_offset;

    // Close door
    set_servo_pos(&sa, position);
    delay(200);
    // Open door to mid
    for (position = closed; position >= mid; position -= 1) {
        set_servo_pos(&sa, position);
        delay(SERV_DELAY_TIME);
    }
    delay(PAUSE_TIME_MID);
    // Open door to Open
    for (position = mid; position > open; position -= 1) {
        set_servo_pos(&sa, position);
        delay(SERV_DELAY_TIME);
    }
    set_servo_off(&sa);
    delay(PAUSE_TIME_OPEN);
    // Close door to Close
    for (position = open; position <= closed; position += 1) {
        set_servo_pos(&sa, position);
        delay(SERV_DELAY_TIME);
    }
    delay(PAUSE_TIME_CLOSED);
    set_servo_off(&sa);
    return 0;
}

int servo_trajectory_open(servo_trajectory_t *st) {
    esp_task_wdt_reset();
    servo_actuator_t sa = st->servo;
    degrees_t closed = st->closed;
    degrees_t open = st->open;
    degrees_t position = closed;
    // Bail if the angle is too small.
    if (closed - mid_offset < open) {
        return -1;
    }
    degrees_t mid = closed - mid_offset;

    // Close door
    set_servo_pos(&sa, position);
    delay(200);
    // Open door to mid
    esp_task_wdt_reset();
    for (position = closed; position >= mid; position -= 1) {
        set_servo_pos(&sa, position);
        delay(SERV_DELAY_TIME);
    }
    delay(PAUSE_TIME_MID);
    esp_task_wdt_reset();
    // Open door to Open
    for (position = mid; position > open; position -= 1) {
        set_servo_pos(&sa, position);
        delay(SERV_DELAY_TIME);
    }
    set_servo_off(&sa);
    return 0;
}

int servo_trajectory_close(servo_trajectory_t *st) {
    servo_actuator_t sa = st->servo;
    degrees_t closed = st->closed;
    degrees_t open = st->open;
    degrees_t position = closed;
    // Bail if the angle is too small.
    if (closed - mid_offset < open) {
        return -1;
    }

    // Close door to Close
    for (position = open; position <= closed; position += 1) {
        set_servo_pos(&sa, position);
        delay(SERV_DELAY_TIME);
    }
    esp_task_wdt_reset();
    delay(PAUSE_TIME_CLOSED);
    set_servo_off(&sa);
    return 0;
}

int servo_trajectory_shut(servo_trajectory_t *st) {
    servo_actuator_t sa = st->servo;
    degrees_t closed = st->closed;
    degrees_t open = st->open;
    degrees_t position = closed;
    // Bail if the angle is too small.
    if (closed - mid_offset < open) {
        return -1;
    }
    degrees_t mid = closed - mid_offset;

    // Close door
    set_servo_pos(&sa, position);
    delay(300);
    set_servo_off(&sa);
    return 0;
}

int set_servo_off(servo_actuator_t *sa) {
    servoshields[sa->servoshield].setPWM(sa->actuator, 0, 0);
    delay(20);
    return 0;
}

int set_servo_pos(servo_actuator_t *sa, degrees_t degrees) {
    servoshields[sa->servoshield].setPWM(sa->actuator, 0, degrees2pwm(degrees));
    delay(2);
    return 0;
}

uint16_t degrees2pwm(degrees_t degrees) {
    return map(degrees, 0, 180, SERVOMIN, SERVOMAX);
    return 0;
}
