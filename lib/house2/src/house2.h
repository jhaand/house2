#include <house2_algo.h>
#include <mcp_gpio_hal.h>
#include <stdint.h>

#ifndef HOUSE2_H
#define HOUSE2_H

#ifdef __cplusplus
extern "C" {
#endif

int read_proximity(uint8_t);
int room_special_case_start(room_number_t);
int room_special_case_stop(room_number_t);

#define PROXIMITY_READ_COUNTS 5

#ifdef __cplusplus
}
#endif

#endif
