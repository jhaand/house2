#include <Arduino.h>
#include <distance_hal.h>
#include <house2.h>
#include <mcp_gpio_hal.h>

//#define DEBUG
const distance_t close_enough = 1500;
#ifdef DEBUG
extern char err_buff[80];
#else
char err_buff[80];
#endif

int read_proximity(uint8_t threshold) {
    static int status = 0;
    static uint8_t som1_present = 0;
    distance_t distance_0 = distance_read(0);
    distance_t distance_1 = distance_read(1);

    if ((distance_0 < close_enough) || (distance_1 < close_enough)) {
        som1_present += 1;
    } else {
        som1_present = 0;
    }
    // The person needs to remain close to the object for several cycles before
    // a positive signal is given.

    if (som1_present >= threshold) {
        som1_present = threshold;
        status = 1;
    } else {
        status = 0;
    }

#ifdef DEBUG
    sprintf(err_buff,
            "distance_0: %d, distance_1: %d, som1_present %d, status: %d",
            distance_0, distance_1, som1_present, status);
#endif
    return status;
}

int room_special_case_start(room_number_t room_number) {
    switch (room_number) {
    case (12):
        break;
    case (16):
        mcp_start_audio_music();
        break;
    case (26):
        mcp_start_audio_fireworks();
        break;
    case (27):
        mcp_start_audio_wind();
        mcp_set_fan(1);
    default:
        break;
    }
    return 0;
}

int room_special_case_stop(room_number_t room_number) {
    switch (room_number) {
    case (12):
        break;
    case (16):
        break;
    case (26):
        break;
    case (27):
        mcp_set_fan(0);
        break;
    default:
        break;
    }
    return 0;
}
