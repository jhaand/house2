#include <sensorbox_algo.h>

#ifdef DEBUG
#include <Arduino.h>
#endif
#ifdef TEST_DEBUG
#include <stdio.h>
#endif

const int room_heights[] = {2055, 1875, 1678, 1485, 1298, 1115,
                            938,  765,  598,  435,  301,  0};

extern char err_buffer[120];

int get_location(sensor_array_t *sa, room_location_t *rl) {
    static const int sensor_array_len =
        sizeof(sa->sensor) / sizeof(sa->sensor[0]);
    static const int room_heights_len =
        sizeof(room_heights) / sizeof(room_heights[0]);
    int house_top = room_heights[0];
    int house_bottom = room_heights[room_heights_len - 1];
    int status = 0;

    uint8_t sensors_counted = 0;
    int sensor_height_tmp = 0;
    int sensor_height_lowest = 3000;
    int sensor_height = 0;

    for (int i = 0; i < sensor_array_len; i++) {
        sensor_height_tmp =
            sa->sensor[i] + SENSOR_PLATE_HEIGHT + SENSOR_HAND_OFFSET;
        if (sensor_height_tmp < sensor_height_lowest) {
            sensor_height_lowest = sensor_height_tmp;
        }
        if (sensor_height_tmp < house_top) {
            status = 1;
            // Set the column to the found sensor
            rl->column = i + 1;
            sensor_height = sensor_height_tmp;
            sensors_counted++;
        }
    }

    if (sensors_counted == 0) {
        rl->column = 0;
        rl->row = 0;
        status = 0;
    } else if (sensors_counted > 1) {
        rl->column = 10;
        sensor_height = sensor_height_lowest;
        status = 2;
    }

    // Search for correct row
    for (int i = 0; i < room_heights_len; i++) {
        int current_height = room_heights[i];
        if (current_height < sensor_height) {
            rl->row = i;
            break;
        }
    }

#ifdef TEST_DEBUG
    printf("\nget_location: sensor_height: %d, column: %d row: %d, "
           "sensors_counted: %d\n",
           sensor_height, rl->column, rl->row, sensors_counted);
#endif
#ifdef DEBUG
    sprintf(err_buffer, "get_location: sensor_height: %d column: %d row: %d\n",
            sensor_height, rl->column, rl->row);
#endif
    return status;
}

room_number_t select_room(room_location_t *rl, uint8_t req_counts) {
    uint8_t room = 0;
    static uint8_t counter = 1;
    static room_number_t room_old = 60;
#ifdef DEBUG
    sprintf(err_buffer, "\ncolumn: %d row: %d\n", rl->column, rl->row);
#endif
#ifdef TEST_DEBUG
    printf("\nselect room: column: %d row: %d\n", rl->column, rl->row);
#endif

    // Magic algorithm to find the correct room.
    if (rl->row == 0) {
        room = 0;
    } else if (rl->row == 1) {
        if (rl->column == 3) {
            room = 1;
        }
    } else if (rl->row == 2) {
        if ((rl->column > 1) && (rl->column < 5)) {
            room = rl->column;
        }
    } else if (rl->row < 11) {
        room = (rl->row - 2) * 5 + rl->column - 1;
    } else {
        room = 50;
    }

    // Sanity check on the columns.
    if (rl->column > 5) {
        room = 50;
    }

    // Just to be sure no funny stuff happens.
    if (req_counts <= 1) {
        room_old = room;
        return room;
    }

    if (room == room_old) {
        counter++;
        if (counter >= req_counts) {
            counter = req_counts;
            return room;
        }
    } else {
        counter = 1;
    }
    room_old = room;
    room = 70;
    return room;
}
