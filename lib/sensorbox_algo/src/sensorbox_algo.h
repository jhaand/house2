#include <house2_algo.h>
#include <stdint.h>
#ifndef SENSORBOX_ALGO_H
#define SENSORBOX_ALGO_H

#ifdef __cplusplus
extern "C" {
#endif

#define SENSOR_PLATE_HEIGHT 130
#define SENSOR_HAND_OFFSET (30)

#ifndef SENSOR_ARRAY_T
#define SENSOR_ARRAY_T
typedef volatile struct { int sensor[5]; } sensor_array_t;
#endif

typedef volatile struct {
    int column;
    int row;
} room_location_t;

int get_location(sensor_array_t *, room_location_t *);
room_number_t select_room(room_location_t *, uint8_t counts);

#ifdef __cplusplus
}
#endif

#endif
