#ifndef DISTANCE_HAL_H
#define DISTANCE_HAL_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef uint16_t distance_t; // Distance in mm.

// Initialize the VL53L1X distance sensors.
int distance_init(void);

// Read the first or the second sensor in house 2
distance_t distance_read(uint8_t);

#ifdef __cplusplus
}
#endif
#endif
