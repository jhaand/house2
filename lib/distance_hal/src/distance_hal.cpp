#include <Arduino.h>
#include <distance_hal.h>

/*
Inspired by code from:
  By: Nathan Seidle SparkFun Electronics Date: April 4th, 2018
  License: This code is public domain but you buy me a beer if you use this and
  we meet someday (Beerware license).
*/

#include "SparkFun_VL53L1X.h" //Click here to get the library: http://librarymanager/All#SparkFun_VL53L1X
#include <Wire.h>

// Optional interrupt and shutdown pins.
#define SHUTDOWN_PIN_0 16
#define SHUTDOWN_PIN_1 17
#define INTERRUPT_PIN_0 -1
#define INTERRUPT_PIN_1 -1

// SFEVL53L1X distanceSensor;
// Uncomment the following line to use the optional shutdown and interrupt pins.
// SFEVL53L1X distanceSensor(Wire, SHUTDOWN_PIN, INTERRUPT_PIN);
SFEVL53L1X distanceSensor0(Wire, SHUTDOWN_PIN_0, INTERRUPT_PIN_0);
SFEVL53L1X distanceSensor1(Wire, SHUTDOWN_PIN_1, INTERRUPT_PIN_1);

// Initialize the VL53L1X distance sensors.
int distance_init(void) {
    int result = 0;
    distanceSensor0.sensorOff();
    delay(100);
    distanceSensor1.sensorOff();
    delay(100);
    distanceSensor0.sensorOn();
    delay(200);
    if (distanceSensor0.begin() != 0) // Begin returns 0 on a good init
    {
        return -1;
    }
    //
    // Set the new address.
    // Shift the address one bit higher because the VL53 counts the upper 7 bits
    // as the base address.
    distanceSensor0.setI2CAddress(0x32 << 1);

    distanceSensor1.sensorOn();
    delay(200);
    distanceSensor1.setI2CAddress(0x33 << 1);
    if (distanceSensor1.begin() != 0) // Begin returns 0 on a good init
    {
        return -2;
    }
    delay(200);

    return result;
}

// Read the first or the second sensor in house 2
distance_t distance_read(uint8_t sensor) {
    distance_t distance = 16000;
    byte sensor_state = 0;

    if (sensor == 0) {
        distanceSensor0.startRanging(); // Write configuration bytes to initiate
                                        // measurement
        while (!distanceSensor0.checkForDataReady()) {
            delay(1);
        }
        distance = distanceSensor0.getDistance(); // Get the result of the
                                                  // measurement from the sensor
        sensor_state = distanceSensor0.getRangeStatus();
        distanceSensor0.clearInterrupt();
        distanceSensor0.stopRanging();
    }

    if (sensor == 1) {
        distanceSensor1.startRanging(); // Write configuration bytes to initiate
                                        // measurement
        while (!distanceSensor1.checkForDataReady()) {
            delay(1);
        }
        distance = distanceSensor1.getDistance(); // Get the result of the
                                                  // measurement from the sensor
        sensor_state = distanceSensor1.getRangeStatus();
        distanceSensor1.clearInterrupt();
        distanceSensor1.stopRanging();
    }
    if (sensor_state != 0) {
        distance = 5000;
    }

    return distance;
}
