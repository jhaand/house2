#include "MCP23017.h"
#include <Arduino.h>
#include <Wire.h>
#include <mcp_gpio_hal.h>
#include <stdint.h>

MCP23017 MCP0(0x20);
MCP23017 MCP1(0x21);

int mcp_gpio_init(void) {
    int result = 0;
    MCP0.begin();
    MCP1.begin();
    uint8_t mcp0_present = 0;
    uint8_t mcp1_present = 0;

    if (MCP0.isConnected()) {
        mcp0_present = 1;
    } else {
        result = -1;
    }
    if (MCP0.isConnected()) {
        mcp1_present = 1;
    } else {
        result -= 2;
    }

    // Set the port to the correct level and orientation.
    MCP0.pinMode(5, OUTPUT);
    MCP0.digitalWrite(5, HIGH);
    MCP0.pinMode(6, OUTPUT);
    MCP0.digitalWrite(6, HIGH);

    MCP1.pinMode(3, OUTPUT);
    MCP1.digitalWrite(3, LOW);
    MCP1.pinMode(5, OUTPUT);
    MCP1.digitalWrite(5, HIGH);
    MCP1.pinMode(6, OUTPUT);
    MCP1.digitalWrite(6, HIGH);
    return result;
}

int mcp_audio_speech(uint8_t active) {
    if (active == 0) {
        MCP0.digitalWrite(6, HIGH);
    } else {
        MCP0.digitalWrite(6, LOW);
    }
    return 0;
}

int mcp_start_audio_music(void) {
    MCP0.digitalWrite(5, LOW);
    delay(100);
    MCP0.digitalWrite(5, HIGH);
    return 0;
}

int mcp_start_audio_wind(void) {
    MCP1.digitalWrite(5, LOW);
    delay(100);
    MCP1.digitalWrite(5, HIGH);
    return 0;
}

int mcp_start_audio_fireworks(void) {
    MCP1.digitalWrite(6, LOW);
    delay(100);
    MCP1.digitalWrite(6, HIGH);
    return 0;
}

int mcp_set_fan(uint8_t level) {
    int result = 0;
    if (level != 0) {
        result = MCP1.digitalWrite(3, HIGH);
    } else {
        result = MCP1.digitalWrite(3, LOW);
    }
    return result;
}
