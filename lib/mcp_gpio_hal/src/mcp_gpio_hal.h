#ifndef MCP_GPIO_HAL_H
#define MCP_GPIO_HAL_H

#include "MCP23017.h"
#include <Arduino.h>
#include <Wire.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

int mcp_gpio_init(void);
int mcp_audio_speech(uint8_t);
int mcp_start_audio_wind(void);
int mcp_start_audio_music(void);
int mcp_start_audio_fireworks(void);
int mcp_set_fan(uint8_t);

#ifdef __cplusplus
}
#endif

#endif
