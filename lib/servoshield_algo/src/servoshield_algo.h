#include <house2_algo.h>
#include <stdint.h>

#ifndef SERVOSHIELD_ALGO_H
#define SERVOSHIELD_ALGO_H

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_ROOMS 44

typedef struct {
    uint8_t index;
    uint8_t sponsor;
    uint8_t closed;
    uint8_t open;
} room_t;

#ifndef SERVO_TRAJECTORY_T
#define SERVO_TRAJECTORY_T
typedef uint8_t servoshield_idx_t;
typedef uint8_t servo_t;
typedef uint8_t degrees_t;

typedef struct {
    servoshield_idx_t servoshield; // i2c address of the servoshield
    servo_t actuator;              // the index of the servo to control.
} servo_actuator_t;

typedef struct {
    servo_actuator_t servo;
    degrees_t closed;
    degrees_t open;
} servo_trajectory_t;
#endif

const room_t rooms[] = {
    {1, 18, 175, 65},  //
    {2, 36, 104, 14},  // Re-opens after close (Accept)
    {3, 5, 106, 21},   //
    {4, 4, 105, 15},   //
    {5, 0, 72, 0},     //
    {6, 33, 112, 19},  //
    {7, 8, 109, 37},   //
    {8, 17, 92, 3},    //
    {9, 35, 119, 20},  //
    {10, 3, 98, 17},   //
    {11, 16, 102, 20}, //
    {12, 44, 85, 0},   //
    {13, 0, 146, 59},  // Shallow room that works excellent
    {14, 0, 97, 14},   //
    {15, 2, 90, 0},    //
    {16, 37, 88, 2},   //
    {17, 10, 129, 24}, //
    {18, 13, 129, 20}, //
    {19, 24, 166, 16}, //
    {20, 20, 120, 5},  //
    {21, 37, 177, 59}, //
    {22, 7, 140, 41},  //
    {23, 19, 104, 8},  //
    {24, 12, 96, 0},   //
    {25, 38, 128, 38}, //
    {26, 42, 108, 26}, //
    {27, 29, 110, 0},  //
    {28, 40, 80, 0},   //
    {29, 15, 123, 20}, //
    {30, 41, 87, 11},  //
    {31, 23, 72, 0},   //
    {32, 9, 95, 7},    //
    {33, 22, 120, 10}, //
    {34, 25, 126, 28}, //
    {35, 31, 173, 62}, // Shallow unit that works really well.
    {36, 14, 95, 8},   //
    {37, 1, 120, 38},  //
    {38, 6, 101, 1},   //
    {39, 34, 112, 7},  //
    {40, 39, 121, 30}, //
    {41, 27, 98, 19},  //
    {42, 11, 140, 44}, //
    {43, 43, 103, 15}, //
    {44, 0, 126, 23}   //
};

int get_servo_settings(room_number_t, servo_trajectory_t *);

int get_servo_actuator(room_number_t, servo_actuator_t *);

#ifdef __cplusplus // cplusplus
}
#endif

#endif // SERVOSHIELD_ALGO_H
