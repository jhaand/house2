#include "house2_algo.h"
#include <servoshield_algo.h>
#include <stdint.h>

// Base address of the servo shields.

int get_servo_settings(room_number_t rn, servo_trajectory_t *st) {
    // Check for out of bounds criteria
    if ((rn < 1) || (rn > MAX_ROOMS)) {
        st->closed = 90;
        st->open = 60;
        st->servo.servoshield = 0;
        st->servo.actuator = 0;
        return -1;
    }

    // Get the correct servo actuator based on room number
    get_servo_actuator(rn, &st->servo);

    // Get the correct open and close angles.
    uint8_t rooms_len = sizeof(rooms) / sizeof(rooms[0]);

    st->closed = rooms[rn - 1].closed;
    st->open = rooms[rn - 1].open;
    return rn - 1;
}

int get_servo_actuator(room_number_t rn, servo_actuator_t *servo) {
    servo->servoshield = 0;
    servo->actuator = 0;

    if ((rn < 1) || (rn > MAX_ROOMS)) {
        return -1;
    }

    uint8_t servo_offset[] = {1, 15, 25, 35, 45};
    uint8_t servo_offset_len = sizeof(servo_offset) / sizeof(uint8_t);
    uint8_t i = 0;

    while (i < servo_offset_len) {
        if (servo_offset[i] > rn) {
            servo->servoshield += i - 1;
            servo->actuator = rn - servo_offset[i - 1];
            break;
        }
        i++;
    }
    return 0;
}
