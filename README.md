# House 2

The software and electrical implementation for the project House 2 from A. Haandrikman-Schraets.

This archive resides at gitlab at the following URL:   
https://gitlab.com/jhaand/house2/

To see how House 2 was built in its enterity and what it looks like. See this presentation: https://www.slideshare.net/jhaand/insidehouse2pdf

Or Andrea Haandrikan's website: https://muralmania.nl/blog/house-2/

If you want to know more about the backgrounds and the original idea look at this Youtube movie.
https://www.youtube.com/watch?v=4b_dldF8BO4

## Documentation

The main product specification for the electrical subsystem can be found here. [(link)](https://docs.google.com/document/d/1rQYR_ho_yx6hR6eLSFsw9j8k8ggcw1m17kjFelk29Es/edit?usp=sharing) 
## Software

The software is build using Platformio. In order to build the software use the following commands.

```
pio update         # Update the build environment 
pio run            # build the software
pio run -t upload  # Upload the firmware to the NodeMCU
pio run -t monitor # Open a miniterm console to interact with the NodeMCU
```

Note: If uploading to the NodeMCU doesn't work, push the IO0 button. (Right to the USB connector)

### Scanning the i2c bus 
This tool will scan the whole i2c bus every 3 seconds. 

```
pio run -e i2c_scan -t upload 
pio run -e i2c_scan -t monitor 
```

### Running the Luna config tool

```
pio run -e luna_config -t upload 
pio run -e luna_config -t monitor 
```

Look in the `./tools` directory and `./platformio.ini` to see which other tools are available. 

### Automatic unit tests. 

The used algorithms have uint tests that run native on Linux. You can run them as follows:
```
pio test -e native
```
### Installing and downloading

Clone the archive from gitlab using:  
Install Platformio according to their website: https://platformio.org. 

`git clone https://gitlab.com/jhaand/house2/`

## Electrical

The i2 addresses from i2c_scan should look like the following:
```
Find the I2C adresses

Scanning....
I2C device found at address 0x10  # unconfigured TF-luna
I2C device found at address 0x20  # MCP23017  0   (hw config)
I2C device found at address 0x21  # MCP23017  1   (hw config) 
- I2C device found at address 0x29  # VL53x1 (Default value without config) Normally not seen.
I2C device found at address 0x32  # VL53x1 (configured 0) Seen after init of application
I2C device found at address 0x33  # VL53x1 (configured 1) Seen after init of application
I2C device found at address 0x40  # Servoshield 0
I2C device found at address 0x41  # Servoshield 1
I2C device found at address 0x42  # Servoshield 2
I2C device found at address 0x43  # Servoshield 3
I2C device found at address 0x60  # TF-Luna 1
I2C device found at address 0x61  # TF-Luna 2
I2C device found at address 0x62  # TF-Luna 3
I2C device found at address 0x63  # TF-Luna 4
I2C device found at address 0x64  # TF-Luna 5
done
```

## Troubleshooting
