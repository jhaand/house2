#include <sensorbox_algo.h>
#include <unity.h>

void setUp(void) {
    // Set stuff up here
}

void tearDown(void) {
    // Clean stuff up here
}

void nothing_there(void) {
    sensor_array_t sa = {3000, 3000, 3000, 3000, 3000};
    room_location_t rl = {0, 0};
    int status = get_location(&sa, &rl);
    room_number_t room = select_room(&rl, 1);

    TEST_ASSERT_EQUAL_INT(0, rl.column);
    TEST_ASSERT_EQUAL_INT(0, rl.row);
    TEST_ASSERT_EQUAL_INT(0, room);
}

void something_nothing_there(void) {
    sensor_array_t sa = {3000, 1500, 3000, 3000, 3000};
    room_location_t rl = {0, 0};
    int status = get_location(&sa, &rl);
    room_number_t room = select_room(&rl, 1);

    TEST_ASSERT_EQUAL_INT(2, rl.column);
    TEST_ASSERT_EQUAL_INT(3, rl.row);
    TEST_ASSERT_EQUAL_INT(6, room);

    sa.sensor[0] = 3000;
    sa.sensor[1] = 3000;
    sa.sensor[2] = 3000;
    sa.sensor[3] = 3000;
    sa.sensor[4] = 3000;
    status = get_location(&sa, &rl);
    room = select_room(&rl, 1);

    TEST_ASSERT_EQUAL_INT(0, rl.column);
    TEST_ASSERT_EQUAL_INT(0, rl.row);
    TEST_ASSERT_EQUAL_INT(0, room);
}

void room_in_the_middle(void) {
    sensor_array_t sa = {3000, 3000, 1030 - SENSOR_PLATE_HEIGHT, 3000, 3000};
    room_location_t rl = {0, 0};
    int status = get_location(&sa, &rl);
    room_number_t room = select_room(&rl, 1);

    TEST_ASSERT_EQUAL_INT(3, rl.column);
    TEST_ASSERT_EQUAL_INT(6, rl.row);
    TEST_ASSERT_EQUAL_INT(22, room);
}

void counted_room_checks(void) {
    // First set it to something different to initialize
    room_location_t rl = {3, 7};
    room_number_t room = select_room(&rl, 5);
    rl.column = 3;
    rl.row = 6;

    room = select_room(&rl, 5);
    TEST_ASSERT_EQUAL_INT(70, room);
    room = select_room(&rl, 5);
    TEST_ASSERT_EQUAL_INT(70, room);
    room = select_room(&rl, 5);
    TEST_ASSERT_EQUAL_INT(70, room);
    room = select_room(&rl, 5);
    TEST_ASSERT_EQUAL_INT(70, room);
    room = select_room(&rl, 5);
    TEST_ASSERT_EQUAL_INT(22, room);
    room = select_room(&rl, 5);
    TEST_ASSERT_EQUAL_INT(22, room);
    room = select_room(&rl, 5);
    TEST_ASSERT_EQUAL_INT(22, room);
}

void room_in_the_top_1(void) {
    sensor_array_t sa = {3000, 3000, 1876, 3000, 3000};
    room_location_t rl = {0, 0};
    int status = get_location(&sa, &rl);
    room_number_t room = select_room(&rl, 1);

    TEST_ASSERT_EQUAL_INT(3, rl.column);
    TEST_ASSERT_EQUAL_INT(1, rl.row);
    TEST_ASSERT_EQUAL_INT(1, room);
}

void single_sensor_bottom(void) {
    sensor_array_t sa = {3000, 3000, 0000, 3000, 3000};
    room_location_t rl = {0, 0};
    int status = get_location(&sa, &rl);
    room_number_t room = select_room(&rl, 1);

    TEST_ASSERT_EQUAL_INT(3, rl.column);
    TEST_ASSERT_EQUAL_INT(11, rl.row);
    TEST_ASSERT_EQUAL_INT(50, room);
}

void multiple_sensors_bottom(void) {
    sensor_array_t sa = {3000, 1030 - SENSOR_PLATE_HEIGHT,
                         1030 - SENSOR_PLATE_HEIGHT, 3000, 3000};
    room_location_t rl = {0, 0};
    int status = get_location(&sa, &rl);
    room_number_t room = select_room(&rl, 1);

    TEST_ASSERT_EQUAL_INT(10, rl.column);
    TEST_ASSERT_EQUAL_INT(6, rl.row);
    TEST_ASSERT_EQUAL_INT(50, room);
}

void multiple_sensors_too_low(void) {
    sensor_array_t sa = {3000, 150 - SENSOR_PLATE_HEIGHT,
                         1030 - SENSOR_PLATE_HEIGHT, 3000, 3000};
    room_location_t rl = {0, 0};
    int status = get_location(&sa, &rl);
    room_number_t room = select_room(&rl, 1);

    TEST_ASSERT_EQUAL_INT(10, rl.column);
    TEST_ASSERT_EQUAL_INT(11, rl.row);
    TEST_ASSERT_EQUAL_INT(50, room);
}

void room_selections(void) {
    volatile room_location_t rl = {1, 10};
    room_number_t room = select_room(&rl, 1);
    TEST_ASSERT_EQUAL_INT(40, room);

    rl.column = 1;
    rl.row = 3;
    room = select_room(&rl, 1);
    TEST_ASSERT_EQUAL_INT(5, room);

    rl.column = 1;
    rl.row = 1;
    room = select_room(&rl, 1);
    TEST_ASSERT_EQUAL_INT(0, room);

    rl.column = 3;
    rl.row = 1;
    room = select_room(&rl, 1);
    TEST_ASSERT_EQUAL_INT(1, room);
}

int main(void) {
    UNITY_BEGIN();
    RUN_TEST(nothing_there);
    RUN_TEST(something_nothing_there);
    RUN_TEST(room_in_the_middle);
    RUN_TEST(counted_room_checks);
    RUN_TEST(room_in_the_top_1);
    RUN_TEST(single_sensor_bottom);
    RUN_TEST(multiple_sensors_bottom);
    RUN_TEST(multiple_sensors_too_low);
    RUN_TEST(room_selections);
    UNITY_END();

    return 0;
}
