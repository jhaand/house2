#include <servoshield_algo.h>
#include <stdio.h>
#include <unity.h>

void setUp(void) {
    // Set stuff up here
}

void tearDown(void) {
    // Clean stuff up here
}

void getting_right_actuator(void) {
    servo_actuator_t sa = {0, 0};
    int result = 0;

    room_number_t room = -20;
    result = get_servo_actuator(room, &sa);
    TEST_ASSERT_EQUAL_INT(-1, result);
    TEST_ASSERT_EQUAL_INT(0, sa.servoshield);
    TEST_ASSERT_EQUAL_INT(0, sa.actuator);

    room = 45;
    result = get_servo_actuator(room, &sa);
    TEST_ASSERT_EQUAL_INT(-1, result);
    TEST_ASSERT_EQUAL_INT(0, sa.servoshield);
    TEST_ASSERT_EQUAL_INT(0, sa.actuator);

    room = 2;
    result = get_servo_actuator(room, &sa);
    TEST_ASSERT_EQUAL_INT(0, result);
    TEST_ASSERT_EQUAL_INT(0, sa.servoshield);
    TEST_ASSERT_EQUAL_INT(1, sa.actuator);

    room = 44;
    result = get_servo_actuator(room, &sa);
    TEST_ASSERT_EQUAL_INT(0, result);
    TEST_ASSERT_EQUAL_INT(3, sa.servoshield);
    TEST_ASSERT_EQUAL_INT(9, sa.actuator);

    room = 14;
    result = get_servo_actuator(room, &sa);
    TEST_ASSERT_EQUAL_INT(0, result);
    TEST_ASSERT_EQUAL_INT(0, sa.servoshield);
    TEST_ASSERT_EQUAL_INT(0x0d, sa.actuator);

    room = 15;
    result = get_servo_actuator(room, &sa);
    TEST_ASSERT_EQUAL_INT(0, result);
    TEST_ASSERT_EQUAL_INT(1, sa.servoshield);
    TEST_ASSERT_EQUAL_INT(0, sa.actuator);

    room = 22;
    result = get_servo_actuator(room, &sa);
    TEST_ASSERT_EQUAL_INT(0, result);
    TEST_ASSERT_EQUAL_INT(1, sa.servoshield);
    TEST_ASSERT_EQUAL_INT(7, sa.actuator);

    room = 34;
    result = get_servo_actuator(room, &sa);
    TEST_ASSERT_EQUAL_INT(0, result);
    TEST_ASSERT_EQUAL_INT(2, sa.servoshield);
    TEST_ASSERT_EQUAL_INT(9, sa.actuator);
}

void get_servo_settings(void) {
    servo_actuator_t sa = {0, 0};
    servo_trajectory_t st = {sa, 0, 0};
    int result = 0;

    room_number_t room = 0;
    result = get_servo_settings(room, &st);
    TEST_ASSERT_EQUAL_INT(-1, result);
    TEST_ASSERT_EQUAL_INT(0, st.servo.servoshield);
    TEST_ASSERT_EQUAL_INT(0, st.servo.actuator);
    TEST_ASSERT_EQUAL_INT(90, st.closed);
    TEST_ASSERT_EQUAL_INT(60, st.open);

    room = 1;
    result = get_servo_settings(room, &st);
    TEST_ASSERT_EQUAL_INT(0, result);
    TEST_ASSERT_EQUAL_INT(0, st.servo.servoshield);
    TEST_ASSERT_EQUAL_INT(0, st.servo.actuator);
    TEST_ASSERT_EQUAL_INT(175, st.closed);
    TEST_ASSERT_EQUAL_INT(65, st.open);

    room = 6;
    result = get_servo_settings(room, &st);
    TEST_ASSERT_EQUAL_INT(5, result);
    TEST_ASSERT_EQUAL_INT(0, st.servo.servoshield);
    TEST_ASSERT_EQUAL_INT(5, st.servo.actuator);
    TEST_ASSERT_EQUAL_INT(112, st.closed);
    TEST_ASSERT_EQUAL_INT(19, st.open);

    room = 24;
    result = get_servo_settings(room, &st);
    TEST_ASSERT_EQUAL_INT(23, result);
    TEST_ASSERT_EQUAL_INT(1, st.servo.servoshield);
    TEST_ASSERT_EQUAL_INT(9, st.servo.actuator);
    TEST_ASSERT_EQUAL_INT(90, st.closed);
    TEST_ASSERT_EQUAL_INT(0, st.open);
}

int main(void) {
    UNITY_BEGIN();
    RUN_TEST(getting_right_actuator);
    RUN_TEST(get_servo_settings);
    UNITY_END();

    return 0;
}
