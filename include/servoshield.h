/* Header file for controlling the servoshields in house 2 */

#ifndef SERVOSHIELD_H
#define SERVOSHIELD_H
#include <stdint.h>

// Public functions
int servos_init(void);
uint16_t degrees2pwm(uint8_t degrees);

#endif // SERVOSHIELD_H
