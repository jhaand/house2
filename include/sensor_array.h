#ifndef SENSOR_ARRAY_H
#define SENSOR_ARRAY_H
#include <stdint.h>

/* De limieten tussen de verschillende kamertjes. Beginnen onder het onderste
 * kamertje en doorgaan tot boven het hoogste kamertje */

uint32_t column_heights[] = {300, 460, 620, 780, 940, 1100, 1260,1420, 1580, 1740}
uint32_t const luna_height_offset = 60; 

uint32_t sensor_heights[] = {0, 0, 0, 0, 0};

struct door {
  uint32_t column;
  uint32_t row; 
  bool detected } detected_door;


int luna_heigths(sensor_heights *int);

int get_detected_door();

#endif /* SENSOR_ARRAY_H */
